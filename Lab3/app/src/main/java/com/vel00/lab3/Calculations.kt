package com.vel00.lab3

import kotlin.math.sqrt

// Функция решения уравнения
fun solve(a: Double, b: Double, c: Double): List<Double> {
    val d = b * b - 4 * a * c

    if (d > 0) {
        val x1 = (-b + sqrt(d)) / (2 * a)
        val x2 = (-b - sqrt(d)) / (2 * a)
        return listOf(x1, x2)
    }
    else if (d == 0.0) {
        val x = (-b) / (2 * a)
        return listOf(x)
    }
    else {
        return listOf()
    }
}

// Функция проверки решения уравнения
fun checkSolution(a: Double, b: Double, c: Double, roots: List<Double>): Boolean {
    val solution = solve(a, b, c)
    val rootSet = roots.map { it.format(2) }.toSet()
    val solutionSet = solution.map { it.format(2) }.toSet()
    return rootSet == solutionSet
}

// Метод для форматирования дробного числа в строку
fun Double.format(p: Int = 2): String {
    return "%.${p}f"
        .format(this)
        .trimEnd('0')
        .trimEnd('.')
}