package com.vel00.lab3

import androidx.compose.material3.FilledIconButton
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

// Модель решения уравнения
@Serializable
data class Solution(val a: Double, val b: Double, val c: Double, val roots: List<Double>, val correct: Boolean)

// Состояние для модели представления
data class SolutionList(
    val solutions: List<Solution> = listOf(),
    val loaded: Boolean = true
)

// Модель представления списка уравнений
class EquationViewModel : ViewModel() {
    // Состояние модели представления
    private val _state = MutableStateFlow(SolutionList())
    // Публичная не изменяемая копия состояния
    val state = _state.asStateFlow()

    // Добавление решения в список решенных уравнений
    fun addSolution(solution: Solution) {
        _state.update {
            it.copy(
                solutions = it.solutions.plus(solution)
            )
        }
    }

    // Сохранение списка решенных уравнений в файл
    fun save(path: String) {
        val data = Json.encodeToString(state.value.solutions)

        // Запуск коррутины в контексте ввода-вывода
        viewModelScope.launch(Dispatchers.IO) {
            File(path).writeText(data)
        }
    }

    // Загрузка списка решенных уравнений из файла
    fun load(path: String) {
        // Помечаем, что загрузка файла выполняется
        _state.update { it.copy(loaded = false) }

        // Запуск коррутины в контексте ввода-вывода
        viewModelScope.launch(Dispatchers.IO) {
            val data = File(path).readText()

            // Выполнение коррутины в главном контексте приложения
            withContext(Dispatchers.Main) {
                val solutions = Json.decodeFromString<List<Solution>>(data)
                _state.update { SolutionList(solutions) }
            }
        }
    }
}

