@file:OptIn(ExperimentalMaterial3Api::class)

package com.vel00.lab3

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.materialIcon
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.vel00.lab3.ui.theme.Lab3Theme
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.math.sqrt

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Создание модели представления
        val viewModel = EquationViewModel()

        // Задаём файл для сохранения решений
        val file = File(filesDir, "solutions.json")
        if (file.exists()) {
            viewModel.load(file.path)
        }

        setContent {
            Lab3Theme {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    // Переменные для работы виджета вкладок
                    var currentTab by remember { mutableStateOf(0) }
                    val tabs = listOf("Уравнение", "Список уравнений")

                    // Текущее состояние модели представления
                    val state = viewModel.state.collectAsState()

                    Column(modifier = Modifier.fillMaxSize()) {
                        // Виджет вкладок
                        TabRow(selectedTabIndex = currentTab) {
                            tabs.forEachIndexed { index, name ->
                                Tab(
                                    text = { Text(name) },
                                    selected = currentTab == index,
                                    onClick = { currentTab = index }
                                )
                            }
                        }
                        if (currentTab == 0) {
                            // Виджет для решения и проверки уравнения
                            Equation(
                                modifier = Modifier
                                    .padding(12.dp)
                                    .fillMaxWidth(),
                                onNewSolution = { solution ->
                                    viewModel.addSolution(solution)
                                    viewModel.save(file.path)
                                },
                            )
                        } else {
                            if (state.value.loaded) {
                                // Список решенных уравнений
                                EquationList(
                                    modifier = Modifier
                                        .padding(12.dp)
                                        .fillMaxWidth(),
                                    solutions = state.value.solutions
                                )
                            } else {
                                Box(
                                    modifier = Modifier.fillMaxSize(),
                                    contentAlignment = Alignment.Center
                                ) {
                                    // Индикатор загрузки файла
                                    CircularProgressIndicator(modifier = Modifier.width(64.dp))
                                }
                            }
                        }
                    }


                }
            }
        }
    }
}

