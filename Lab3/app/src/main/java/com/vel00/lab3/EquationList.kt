package com.vel00.lab3

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

// Виджет в списке уравнений
@Composable
fun EquationList(
    modifier: Modifier = Modifier,
    solutions: List<Solution> = listOf(),
) {
    Column(modifier = modifier) {
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            items(solutions.reversed()) { solution ->
                EquationCard(solution = solution, modifier = Modifier.fillMaxWidth())
            }
        }
    }
}

//Виджет карточки решения уравнений
@Composable
fun EquationCard(
    solution: Solution,
    modifier: Modifier = Modifier,
) {
    Card(
        modifier = modifier,
    ) {
        Row(
            modifier = Modifier.padding(12.dp),
        ) {
            // Уравнение
            Text("${solution.a.format(2)}x^2 + ${solution.b.format(2)}x + ${solution.c.format(2)} = 0")
            var result = ""
            when (solution.roots.size) {
                2 -> {
                    val (x1, x2) = solution.roots
                    result = "x1 = ${ x1.format(2) }, x2 = ${ x2.format(2) }"
                }
                1 -> {
                    val x = solution.roots[0]
                    result = "x = ${ x.format(2) }"
                }
                0 -> {
                    result = "Нет корней"
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            // Решение пользователя
            Text(result)
            Spacer(modifier = Modifier.weight(1f))
            // Флаг указания верно/неверно уравнения
            Text( if (solution.correct) "Верно" else "Неверно")
        }
    }
}