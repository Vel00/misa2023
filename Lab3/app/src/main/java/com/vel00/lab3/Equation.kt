@file:OptIn(ExperimentalMaterial3Api::class)

package com.vel00.lab3

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.vel00.lab3.ui.theme.Lab3Theme

// Виджет решения и проверки уравнения
@Composable
fun Equation(
    modifier: Modifier = Modifier,
    onNewSolution: (Solution) -> Unit = {},
) {
    // Коэффициенты уравения
    var a by remember { mutableStateOf("") }
    var b by remember { mutableStateOf("") }
    var c by remember { mutableStateOf("") }
    // Отображаемый текст решения уравнения
    var solution by remember { mutableStateOf("") }
    // Корни уравнения
    var x1 by remember { mutableStateOf("") }
    var x2 by remember { mutableStateOf("") }
    // Текст проверки уравнения
    var check by remember { mutableStateOf("") }


    Column(modifier.verticalScroll(rememberScrollState())) {
        // Ввод корней уравнения
        InputField(name = "a", getValue = a, setValue = { a = it })
        Spacer(modifier = Modifier.height(12.dp))
        InputField(name = "b", getValue = b, setValue = { b = it })
        Spacer(modifier = Modifier.height(12.dp))
        InputField(name = "c", getValue = c, setValue = { c = it })
        Spacer(modifier = Modifier.height(12.dp))

        Button(onClick = {
            if (!validateValue(a)) return@Button
            if (!validateValue(b)) return@Button
            if (!validateValue(c)) return@Button

            val result = solve(a.toDouble(), b.toDouble(), c.toDouble())

            when (result.size) {
                2 -> {
                    val (x1, x2) = result
                    solution = "x1 = ${ x1.format(2) }, x2 = ${ x2.format(2) }"
                }
                1 -> {
                    val x = result[0]
                    solution = "x = ${ x.format(2) }"
                }
                0 -> {
                    solution = "Уравнение не имеет действительных корней"
                }
            }
        }) { Text(text = "Решить") }
        Spacer(modifier = Modifier.height(12.dp))
        // Вывод решения уравнения
        Text(text = "Решение: $solution", style = MaterialTheme.typography.titleLarge)
        Spacer(modifier = Modifier.height(24.dp))
        // Ввод корней уравнения
        InputField(name = "x1", getValue = x1, setValue = { x1 = it })
        Spacer(modifier = Modifier.height(12.dp))
        InputField(name = "x2", getValue = x2, setValue = { x2 = it })
        Spacer(modifier = Modifier.height(12.dp))

        Button(onClick = {
            if (!validateValue(a)) return@Button
            if (!validateValue(b)) return@Button
            if (!validateValue(c)) return@Button

            val roots = mutableListOf<Double>()
            if (validateValue(x1)) roots.add(x1.toDouble())
            if (validateValue(x2)) roots.add(x2.toDouble())

            val result = checkSolution(a.toDouble(), b.toDouble(), c.toDouble(), roots)
            if (result) {
                check = "Верное решение"
            } else {
                check = "Неверное решение"
            }

            val s = Solution(a.toDouble(), b.toDouble(), c.toDouble(), roots, result)
            onNewSolution(s)

        }) { Text(text = "Проверить решение") }
        Spacer(modifier = Modifier.height(12.dp))
        Text(text = check, style = MaterialTheme.typography.titleLarge)
    }

}

// Валидация ввода пользователем чисел
fun validateValue(value: String): Boolean {
    val regex = "^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$".toRegex()
    return regex.matches(value)

}

// Поле ввода чисел с валидацией
@Composable
fun InputField(name: String, getValue: String, setValue: (String) -> Unit) {
    TextField(
        label = { Text(text = "$name = ", style = MaterialTheme.typography.titleLarge)},
        value = getValue,
        onValueChange = { setValue(it) },

        modifier = Modifier.fillMaxWidth(),

    )
}