import json
import requests

# cur = int(input('Enter rating: '))
# req = False
# try:
#     req = requests.get(
#         'https://codeforces.com/api/user.ratedList?activeOnly=false&includeRetired=false')
# except:
#     print('Error requesting CF API')
# if req.status_code == 200 and 'json' in req.headers['content-type']:
#     users = req.json()["result"]
#     users_d = {u["handle"]: (u["rating"], u["rank"]) for u in users}
#     #print('tourist', users_d['tourist'])
#     #print('Всего:', len(users_d), '\n')
#     ranks = {u["rank"]: 0 for u in users}
#     sm = 0
#     for r in ranks:
#         ranks[r] = sum(1 for _ in [u[1]
#                        for u in users_d.values() if u[1] == r])
#         print(r, ';', ranks[r])
#         sm += ranks[r]
#     # print(ranks)
#     print('\nTotal ;', sm)
#     print('\nnewbie_' + str(cur), ';', sum(1 for _ in [u[1]
#           for u in users_d.values() if u[1] == 'newbie' and u[0] >= cur]))
# else:
#     print('Not a JSON responce requesting CF API')


def main():
    try:
        # получение полной информации о пользователях сайта
        response = requests.get('https://codeforces.com/api/user.ratedList?activeOnly=false&includeRetired=false')
    except:
        # сообщение об ошибке при запросе к API
        print("Error requesting CF API")
        return

    # проверка на успешность запроса
    if response.status_code != 200:
        print("Error requesting CF API")
        return
    data = response.json()["result"]

    ranks = {}

    # получение суммарного рейтинга и числа пользователей для каждого ранга
    for user in data:
        rank_name = user["rank"]
        rating = user["rating"]

        # проверка, что ранг уже существует в словаре
        if rank_name in ranks:
            total_rating, count = ranks[rank_name]
        else:
            # задание начальных значений для нового ранга
            total_rating, count = 0, 0

        total_rating += rating
        count += 1

        # сохранение результатов в словарь
        ranks[rank_name] = total_rating, count

    # подсчёт среднего рейтинга пользователей в каждом ранге
    for rank_name, (total_rating, count) in ranks.items():
        mean = total_rating // count
        print(f"{rank_name}: {mean}")


if __name__ == '__main__':
    main()






