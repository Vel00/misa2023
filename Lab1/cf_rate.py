from fastapi import FastAPI, APIRouter
import requests

app = FastAPI(
    title="cf_rate API", openapi_url="/openapi.json"
)

api_router = APIRouter()

@api_router.get("/cf_rate/{cf_id}", status_code=200)
def root(cf_id):
    """
    Root Get
    """
    
    rate = "0"
    try:
        req = requests.get('https://codeforces.com/api/user.rating?handle=' + cf_id)
        if req.status_code == 200 and 'json' in req.headers['content-type']:
            rate = req.json()["result"][-1]["newRating"]
    except:
        rate = "0"
        
    return rate


@api_router.get("/user_address/{handle}")
def user_address(handle):
    try:
        # получение ответа по заданному запросу на сайте codeforces.com
        response = requests.get('https://codeforces.com/api/user.info', params={"handles": handle})
    except Exception:
        return "Ошибка сети"

    # проверка, что пользователь существует
    if response.status_code != 200:
        return "Пользователь не найден"

    data = response.json()["result"][0]

    # проверка наличия у пользователя в данных страны и города
    if "country" not in data or "city" not in data:
        return "Пользователь не указал адрес"

    return data["country"] + " " + data["city"]


app.include_router(api_router)


if __name__ == "__main__":
    # Use this for debugging purposes only
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8001, log_level="debug")