from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as login_user, logout as logout_user
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from .helpers import solve_equation
from .models import Solution


def index(request):
    """Главная страница"""

    return render(request, 'equation/index.html')


def login(request):
    """Страница авторизации/регистрации"""

    # Выбор между страницей регистрации/авторизации
    signup = request.GET.get('signup', False)

    error = ''

    # Передача данных формы для авторизации/регистрации
    if request.method == 'POST':
        try:
            user = User.objects.get(username=request.POST['username'])
        except ObjectDoesNotExist:
            user = None

        # Попытка регистрации
        if signup:
            if user is None:
                User.objects.create_user(
                    username=request.POST['username'],
                    email=request.POST['email'],
                    password=request.POST['password'],
                )
                return redirect('login')
            else:
                error = 'Пользователь с таким именем уже существует'

        # Попытка авторизации
        else:
            if user is not None:
                user = authenticate(
                    username=request.POST['username'],
                    password=request.POST['password'],
                )
            if user is None:
                error = 'Неверное имя пользователя или пароль'
            else:
                login_user(request, user)
                return redirect('index')

    return render(request, 'equation/login.html', {'signup': signup, 'error': error})


def logout(request):
    logout_user(request)

    return redirect('index')


def solve(request):
    """Маршрут решения квадратного уравнения"""

    a = float(request.POST['a'])
    b = float(request.POST['b'])
    c = float(request.POST['c'])

    solution = solve_equation(a, b, c)

    match solution:
        case None:
            result = 'Уравнение не имеет действительных корней'
        case (x1, x2):
            result = f'X1 = {x1:.2f}, X2 = {x2:.2f}'
        case x:
            result = f'X = {x:.2f}'

    return HttpResponse(result)


def check(request):
    """Маршрут проверки правильности решения уравнения"""

    a = float(request.POST['a'])
    b = float(request.POST['b'])
    c = float(request.POST['c'])

    no_solution = bool(request.POST.get('no_solution', False))

    if no_solution:
        x1 = None
        x2 = None
    else:
        x1 = float(request.POST.get('x1'))
        x2 = float(request.POST.get('x2'))

    solution = Solution(a=a, b=b, c=c, x1=x1, x2=x2)
    if request.user.is_authenticated:
        solution.user = request.user
        solution.save()
    else:
        solution.check_solution()

    if solution.correct:
        return HttpResponse('Верное решение')
    else:
        return HttpResponse('Неверное решение')


def solutions(request):
    """Страница решенных уравнений"""

    if not request.user.is_authenticated:
        return redirect('login')

    # Формирование списка решений из БД и отображение их на странице
    solution_list = Solution.objects.filter(user=request.user).order_by('-id')
    return render(request, 'equation/solutions.html', {'solutions': solution_list})

