from django.db import models
from django.contrib.auth.models import User

from .helpers import check_solution


class Solution(models.Model):
    """Решение уравнения, предоставленное пользователем"""

    # Коэффициенты уравнения
    a: float = models.FloatField(default=1)
    b: float = models.FloatField(default=1)
    c: float = models.FloatField(default=1)

    # Корни уравнения
    x1: float = models.FloatField(blank=True, null=True)
    x2: float = models.FloatField(blank=True, null=True)

    # Флаг корректности корней уравнения
    _correct: bool = models.BooleanField()

    # Пользователь, проверивший решение
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='solution')

    @property
    def has_solutions(self) -> bool:
        """Заданы ли корни"""

        return self.x1 is not None or self.x2 is not None

    @property
    def correct(self) -> bool:
        """Корректно ли решение"""

        return self._correct

    def save(self, *args, **kwargs):
        self.check_solution()
        super().save(*args, **kwargs)

    def check_solution(self):
        """Задание флага корректности корней уравнения"""

        if self.x1 is not None:
            self.x1 = round(self.x1, 2)
        if self.x2 is not None:
            self.x2 = round(self.x2, 2)

        self._correct = check_solution(self.a, self.b, self.c, self.x1, self.x2)
