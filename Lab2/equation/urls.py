from django.urls import path

from .views import index, login, logout, solve, check, solutions

urlpatterns = [
    path('', index, name='index'),
    path('login', login, name='login'),
    path('logout', logout, name='logout'),
    path('solve', solve, name='solve'),
    path('check', check, name='check'),
    path('solutions', solutions, name='solutions'),

]

