from math import sqrt


def solve_equation(a, b, c):
    """Нахождение корней квадратного уравнения"""

    d = b ** 2 - 4 * a * c

    if d > 0:
        x1 = (-b + sqrt(d)) / (2 * a)
        x2 = (-b - sqrt(d)) / (2 * a)
        return (x1, x2)
    elif d == 0:
        x = (-b) / (2 * a)
        return x
    else:
        return None


def check_solution(a, b, c, x1, x2):
    """Проверка правильности решения квадратного уравнения"""

    no_solution = x1 is None and x2 is None
    solution = solve_equation(a, b, c)

    correct = False

    # Нет решений
    if no_solution and solution is None:
        correct = True
    # Один корень
    elif isinstance(solution, float):
        solution = round(solution, 2)

        if x1 == x2 == solution:
            correct = True
    # Два корня
    else:
        # Распаковка картежа в две переменные и округление до двух знаков
        y1, y2 = solution
        y1 = round(y1, 2)
        y2 = round(y2, 2)

        # Порядок корней не имеет значения
        if (x1 == y1 and x2 == y2) or (x1 == y2 and x2 == y1):
            correct = True

    return correct
